package com.qiuyu.android.hydrant.app.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.qiuyu.android.hydrant.app.R;
import com.qiuyu.android.hydrant.app.model.News;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ttonway on 16/3/10.
 */
public class FragmentHome extends Fragment {

    ListView mListView;
    NewsAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListView = (ListView) view.findViewById(R.id.listView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                News news = (News) parent.getItemAtPosition(position);
                if (news != null) {
                    Intent intent = new Intent(getActivity(), ActivityWeb.class);
                    intent.setData(Uri.parse(news.getUrl()));
                    startActivity(intent);
                }
            }
        });

        View header = LayoutInflater.from(getActivity()).inflate(R.layout.list_header_news, null);
        mListView.addHeaderView(header);

        List<News> list = new ArrayList<News>();
        list.add(new News("薛军毅：烈火筑真情", "21岁时，他从火场转移出194个液化气瓶；18年间，1000余次火灾现场都有他忙碌的身影；13年里，他寄出了133张汇款单撑起两个孩子的未来", "http://119.china.com.cn/jdxw/txt/2016-03/03/content_8608192.htm"));
        list.add(new News("江苏省公安消防总队部署全年消防工作", "江苏省公安消防总队召开党委扩大会议，回顾去年工作情况，分析当前消防安全形势，部署今年工作任务。省公安厅副厅长程建东出席会议并讲话。省公安消防总队总队长周详传达了省委常委、政法委书记、公安厅厅长王立科对消防工作的重要批示，并作工作报告。", "http://119.china.com.cn/jdxw/txt/2016-03/10/content_8625068.htm?plg_nld=1&plg_uin=1&plg_auth=1&plg_nld=1&plg_usr=1&plg_vkey=1&plg_dev=1"));
        mAdapter = new NewsAdapter(getActivity(), list);
        mListView.setAdapter(mAdapter);
    }

    private class NewsAdapter extends ArrayAdapter<News> {
        LayoutInflater mInflater;

        public NewsAdapter(Context context, List<News> objects) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_item_news, parent, false);
            }
            TextView tv1 = (TextView) convertView.findViewById(R.id.text1);
            TextView tv2 = (TextView) convertView.findViewById(R.id.text2);

            News news = getItem(position);
            tv1.setText(news.getTitle());
            tv2.setText(news.getDescription());

            return convertView;
        }
    }
}
