package com.qiuyu.android.hydrant.app.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.qiuyu.android.hydrant.app.R;
import com.qiuyu.android.hydrant.app.ui.dialog.ProgressDialogFrag;

/**
 * Created by ttonway on 16/3/10.
 */
public class ActivityLogin extends BaseActivity implements View.OnClickListener {

    Button mLoginButton;
    Button mRegisterButton;

    LoginTask mLoginTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginButton = (Button) findViewById(R.id.btn_login);
        mRegisterButton = (Button) findViewById(R.id.btn_register);
        mLoginButton.setOnClickListener(this);
        mRegisterButton.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mLoginTask != null) {
            mLoginTask.cancel(true);
            mLoginTask = null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
            case R.id.btn_register:
                if (mLoginTask != null) {
                    mLoginTask.cancel(true);
                    mLoginTask = null;
                }
                mLoginTask = new LoginTask();
                mLoginTask.execute();
                break;
        }
    }


    private class LoginTask extends AsyncTask<Void, Void, Void> {

        ProgressDialogFrag mProgressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialogFrag.newInstance("请稍候");
            mProgressDialog.show(getSupportFragmentManager(), "progress");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mLoginTask = null;
            mProgressDialog.dismissAllowingStateLoss();

            Intent intent = new Intent(ActivityLogin.this, ActivityMain.class);
            startActivity(intent);
            finish();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mLoginTask = null;
            mProgressDialog.dismissAllowingStateLoss();
        }
    }
}
