package com.qiuyu.android.hydrant.app.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.TabHost;
import com.qiuyu.android.hydrant.app.R;
import com.qiuyu.android.hydrant.app.widget.TabManager;


public class ActivityMain extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    static final int TAB_COUNT = 4;
    static int[] TAB_ICONS = new int[]{R.drawable.ic_tab1, R.drawable.ic_tab2, R.drawable.ic_tab3, R.drawable.ic_tab4};
    static int[] TAB_TITLES = new int[]{R.string.tab1, R.string.tab2, R.string.tab3, R.string.tab4};
    static Class<?>[] TAB_CLS = new Class[]{FragmentHome.class, FragmentHydrantMap.class, FragmentHydrantList.class, FragmentSetting.class};
    static String[] TAB_TAGS = new String[]{"home", "map", "list", "setting"};

    TabHost mTabHost;
    TabManager mTabManager;
    RadioGroup mRadioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        mTabManager = new TabManager(this, this.getSupportFragmentManager(),
                mTabHost, R.id.realtabcontent);
        for (int i = 0; i < TAB_COUNT; i++) {
            String title = getString(TAB_TITLES[i]);
            Drawable icon = getResources().getDrawable(TAB_ICONS[i]);
            mTabManager.addTab(
                    mTabHost.newTabSpec(TAB_TAGS[i]).setIndicator(title, icon),
                    TAB_CLS[i], null);
        }

        mRadioGroup = (RadioGroup) findViewById(R.id.main_radio);
        mRadioGroup.setOnCheckedChangeListener(this);
        mRadioGroup.check(R.id.radio0);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int index = -1;
        switch (checkedId) {
            case R.id.radio0:
                index = 0;
                break;
            case R.id.radio1:
                index = 1;
                break;
            case R.id.radio2:
                index = 2;
                break;
            case R.id.radio3:
                index = 3;
                break;
        }

        if (index != -1) {
            setTitle(TAB_TITLES[index]);
            mTabHost.setCurrentTabByTag(TAB_TAGS[index]);
        }
    }
}
