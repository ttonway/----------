package com.qiuyu.android.hydrant.app.model;

/**
 * Created by ttonway on 16/3/10.
 */
public class Hydrant {
    String code;
    String address;
    String status;
    String using;

    public Hydrant(String code, String address, String status, String using) {
        this.code = code;
        this.address = address;
        this.status = status;
        this.using = using;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsing() {
        return using;
    }

    public void setUsing(String using) {
        this.using = using;
    }
}
