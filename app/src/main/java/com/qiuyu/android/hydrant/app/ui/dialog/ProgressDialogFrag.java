package com.qiuyu.android.hydrant.app.ui.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created with IntelliJ IDEA.
 * User: ttonway
 * Date: 14-7-29
 * Time: 下午2:17
 */
public class ProgressDialogFrag extends DialogFragment {
    private static final String KEY_MESSAGE = "ProgressDialogFrag:msg";

    public static ProgressDialogFrag newInstance() {
        return newInstance("Waiting...");
    }

    public static ProgressDialogFrag newInstance(String message) {
        ProgressDialogFrag frag = new ProgressDialogFrag();
        Bundle b = new Bundle();
        b.putString(KEY_MESSAGE, message);
        frag.setArguments(b);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getArguments().getString(KEY_MESSAGE));
        dialog.setIndeterminate(true);
        return dialog;
    }
}
