package com.qiuyu.android.hydrant.app.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.qiuyu.android.hydrant.app.R;
import com.qiuyu.android.hydrant.app.model.Hydrant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ttonway on 16/3/10.
 */
public class FragmentHydrantList extends Fragment {

    ListView mListView;
    NewsAdapter mAdapter;

    MenuItem mSearhMenuItem;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_hydrant_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListView = (ListView) view.findViewById(R.id.listView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Hydrant hydrant = (Hydrant) parent.getItemAtPosition(position);
                if (hydrant != null) {
                    //TODO
                }
            }
        });

        List<Hydrant> list = new ArrayList<Hydrant>();
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        list.add(new Hydrant("PKJ00157", "南京信息工程科学信息中心", "良好", "未使用"));
        mAdapter = new NewsAdapter(getActivity(), list);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.hydrant_list, menu);
        mSearhMenuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(mSearhMenuItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();

        mSearhMenuItem = null;
    }

    private class NewsAdapter extends ArrayAdapter<Hydrant> {
        LayoutInflater mInflater;

        public NewsAdapter(Context context, List<Hydrant> objects) {
            super(context, 0, objects);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_item_hydrant, parent, false);
            }
            TextView tv1 = (TextView) convertView.findViewById(R.id.text1);
            TextView tv2 = (TextView) convertView.findViewById(R.id.text2);
            TextView tv3 = (TextView) convertView.findViewById(R.id.text3);
            TextView tv4 = (TextView) convertView.findViewById(R.id.text4);

            Hydrant hydrant = getItem(position);
            tv1.setText(hydrant.getCode());
            tv2.setText(hydrant.getAddress());
            tv3.setText(hydrant.getStatus());
            tv4.setText(hydrant.getUsing());

            return convertView;
        }
    }
}
