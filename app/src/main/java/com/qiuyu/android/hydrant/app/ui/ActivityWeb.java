package com.qiuyu.android.hydrant.app.ui;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.qiuyu.android.hydrant.app.R;
import com.qiuyu.android.hydrant.app.utils.Utils;

/**
 * Created by ttonway on 16/3/10.
 */
public class ActivityWeb extends BaseActivity {

    WebView mWebView;
    boolean mIsPaused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);

                ActivityWeb.this.setTitle(title);
            }

        });

        Uri data = getIntent().getData();
        mWebView.loadUrl(data.toString());
    }

    @TargetApi(11)
    @Override
    protected void onResume() {
        super.onResume();
        if (mWebView != null && mIsPaused) {
            mWebView.reload();
            if (Utils.hasHoneycomb()) {
                mWebView.onResume();
            }
            mWebView.resumeTimers();
        }
        mIsPaused = false;
    }

    @TargetApi(11)
    @Override
    protected void onPause() {
        super.onPause();
        mIsPaused = true;
        if (mWebView != null) {
            mWebView.stopLoading();
            if (Utils.hasHoneycomb()) {
                mWebView.onPause(); //pauses background threads, stops playing sound
            }
            mWebView.pauseTimers(); //pauses the WebViewCore
        }
    }
}
