package com.qiuyu.android.hydrant.app;

import android.app.Application;
import com.baidu.mapapi.SDKInitializer;

/**
 * Created by ttonway on 16/3/10.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        SDKInitializer.initialize(this);
    }
}
